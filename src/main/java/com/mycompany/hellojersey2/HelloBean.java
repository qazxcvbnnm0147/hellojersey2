/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hellojersey2;

/**
 *
 * @author user
 */
public class HelloBean {
    private String helloString=null;
    private long time=-1;
    public String getHelloString(){
        return helloString;
    }
    public void setHelloString(String helloString){
        this.helloString = helloString;
    }
    public long getTime(){
        return time;
    }
    public void setTime(long time){
        this.time = time;
    }
}
