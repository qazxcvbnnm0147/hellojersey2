/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.hellojersey2;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author user
 */
@Path("test")
public class Test {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("name/{u}")
    public HelloBean sayHello(@PathParam("u") String name){
        HelloBean b=new HelloBean();
        b.setHelloString("Hello, "+name);
        b.setTime(System.currentTimeMillis());
        return b;
    }
    
}
